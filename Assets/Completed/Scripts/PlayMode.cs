﻿using UnityEngine;

public class PlayMode
{
    public int difficultyRangeLevel1Min;
    public int difficultyRangeLevel1Max;
    public int difficultyRangeLevel2Min;
    public int difficultyRangeLevel2Max;

    public int level1ChanceRangeLow;
    public int level1ChanceRangeHigh;
    public int level2ChanceRangeLow;
    public int level2ChanceRangeHigh;

    public PlayMode(difficulty mode, int columns, int rows)
    {
        int minRange = 0;
        int maxRange = Mathf.Max(columns, rows);

        switch(mode)
        {
            case difficulty.Easy:
            {
                difficultyRangeLevel1Min = minRange;
                difficultyRangeLevel1Max = 75;
                difficultyRangeLevel2Min = 76;
                difficultyRangeLevel2Max = maxRange;

                level1ChanceRangeLow = 0;
                level1ChanceRangeHigh = 90;
                level2ChanceRangeLow = 91;
                level2ChanceRangeHigh = 100;
            }
            break;
            case difficulty.Medium:
            {
                difficultyRangeLevel1Min = minRange;
                difficultyRangeLevel1Max = 50;
                difficultyRangeLevel2Min = 51;
                difficultyRangeLevel2Max = maxRange;

                level1ChanceRangeLow = 0;
                level1ChanceRangeHigh = 50;
                level2ChanceRangeLow = 51;
                level2ChanceRangeHigh = 100;
                }
            break;
            case difficulty.Hard:
            {
                difficultyRangeLevel1Min = minRange;
                difficultyRangeLevel1Max = 10;
                difficultyRangeLevel2Min = 11;
                difficultyRangeLevel2Max = maxRange;

                level1ChanceRangeLow = 0;
                level1ChanceRangeHigh = 25;
                level2ChanceRangeLow = 26;
                level2ChanceRangeHigh = 100;
                }
            break;
        }
    }
}
