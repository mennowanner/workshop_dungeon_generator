﻿using UnityEngine;

public class Room
{
    public int roomLevel = 0;                // The level of the room.
    public int xPos;                      // The x coordinate of the lower left tile of the room.
    public int yPos;                      // The y coordinate of the lower left tile of the room.
    public int roomWidth;                 // How many tiles wide the room is.
    public int roomHeight;                // How many tiles high the room is.
    public Direction enteringCorridor;    // The direction of the corridor that is entering this room.


    // This is used for the first room.  It does not have a Corridor parameter since there are no corridors yet.
    public void SetupRoom(IntRange widthRange, IntRange heightRange, int columns, int rows)
    {
        // Set a random width and height.
        roomWidth = widthRange.Random;
        roomHeight = heightRange.Random;

        // Set the x and y coordinates so the room is roughly in the middle of the board.
        xPos = Mathf.RoundToInt(columns / 2f - roomWidth / 2f);
        yPos = Mathf.RoundToInt(rows / 2f - roomHeight / 2f);
    }


    // This is an overload of the SetupRoom function and has a corridor parameter that represents the corridor entering the room.
    public void SetupRoom(IntRange widthRange, IntRange heightRange, int columns, int rows, Corridor corridor)
    {
        // Set the entering corridor direction.
        enteringCorridor = corridor.direction;

        // Set random values for width and height.
        roomWidth = widthRange.Random;
        roomHeight = heightRange.Random;

        switch (corridor.direction)
        {
            // If the corridor entering this room is going north...
            case Direction.North:
                // ... the height of the room mustn't go beyond the board so it must be clamped based
                // on the height of the board (rows) and the end of corridor that leads to the room.
                roomHeight = Mathf.Clamp(roomHeight, 1, rows - corridor.EndPositionY);

                // The y coordinate of the room must be at the end of the corridor (since the corridor leads to the bottom of the room).
                yPos = corridor.EndPositionY;

                // The x coordinate can be random but the left-most possibility is no further than the width
                // and the right-most possibility is that the end of the corridor is at the position of the room.
                xPos = Random.Range(corridor.EndPositionX - roomWidth + 1, corridor.EndPositionX);

                // This must be clamped to ensure that the room doesn't go off the board.
                xPos = Mathf.Clamp(xPos, 0, columns - roomWidth);
                break;
            case Direction.East:
                roomWidth = Mathf.Clamp(roomWidth, 1, columns - corridor.EndPositionX);
                xPos = corridor.EndPositionX;

                yPos = Random.Range(corridor.EndPositionY - roomHeight + 1, corridor.EndPositionY);
                yPos = Mathf.Clamp(yPos, 0, rows - roomHeight);
                break;
            case Direction.South:
                roomHeight = Mathf.Clamp(roomHeight, 1, corridor.EndPositionY);
                yPos = corridor.EndPositionY - roomHeight + 1;

                xPos = Random.Range(corridor.EndPositionX - roomWidth + 1, corridor.EndPositionX);
                xPos = Mathf.Clamp(xPos, 0, columns - roomWidth);
                break;
            case Direction.West:
                roomWidth = Mathf.Clamp(roomWidth, 1, corridor.EndPositionX);
                xPos = corridor.EndPositionX - roomWidth + 1;

                yPos = Random.Range(corridor.EndPositionY - roomHeight + 1, corridor.EndPositionY);
                yPos = Mathf.Clamp(yPos, 0, rows - roomHeight);
                break;
        }
    }

    public void setDifficulty(int xBegin, int yBegin, PlayMode mode)
    {
        //check if the room is in range of a certain level specification.
        if(is_in_range(xBegin, yBegin, mode.difficultyRangeLevel1Min, mode.difficultyRangeLevel1Max))
        {
            roomLevel = 1;
        }
        else if(is_in_range(xBegin, yBegin, mode.difficultyRangeLevel2Min, mode.difficultyRangeLevel2Max))
        {
            roomLevel = 2;
        }

        //To create some challenges in the lower levels we will pick a random number. 
        //This number will or will not change the difficulty based on the number picked.
        //int randomNumber = Random.Range(0, 100);

        //if(randomNumber > mode.level1ChanceRangeLow && randomNumber <= mode.level1ChanceRangeHigh)
        //{
        //    roomLevel = 1;
        //}
        //else if(randomNumber > mode.level2ChanceRangeLow && randomNumber <= mode.level2ChanceRangeHigh)
        //{
        //    roomLevel = 2;
        //}
    }


    public bool is_in_range(int xBegin, int yBegin, int minRange, int maxRange)
    {
        bool is_in_range = false;

        //Get the middle point of the room of x
        int xMidCoord = xPos + (roomWidth / 2);
        int xDistance = Mathf.Abs(xMidCoord - xBegin);

        //Get the middel point of the room of y
        int yMidCoord = yPos + (roomHeight / 2);
        int yDistance = Mathf.Abs(yMidCoord - yBegin);

        //Get the diagonal distance between begin point and room.
        float diagonalDistance = Mathf.Sqrt(Mathf.Pow(yDistance, 2) + Mathf.Pow(xDistance, 2));

        //Check if the tile is between the in and maxrange of a level specification.
        if (diagonalDistance >= minRange && diagonalDistance < maxRange)
        {
            is_in_range = true;
        }

        return is_in_range;
    }
}